Version 0.6 of Energy Based Multiple Model Fitting for Non-Rigid Structure from Motion

The code is currently licenced for non-commercial use only. 
Other licences can be arranged on contact with the authors.

The code is an alpha release, currently both successful use of the
code, and failure cases are of interest to the authors.  Feel free to
contact Chris on crussell@cs.ucl.ac.uk with use cases, bugs or
queries.

 Please cite
@inproceedings{Russell:etal:CVPR2011, author = {Chirs Russell and Joao
Fayad and Lourdes Agapito}, title = {Energy Based Multiple Model
Fitting for Non-Rigid Structure from Motion}, booktitle = cvpr2011,
year = {2011} }

where appropriate. 

More details about the model, and the optimisation performed can be found in the text above.

Thanks to Markus Moll for submitting a bug-fix, and various other people for reporting all of the problems with windows compilation.
They should all be fixed now.

INSTRUCTIONS
------------

From matlab first run compile to create the file

multi.mex*

The command multi is called with the syntax:

[models internal]=multi(costs,neighbours,internal,lambda,MDL,Threshold)

where

* Costs is points by models matrix showing the cost of assigning a
point to a model. All costs must be non-negative.

* Neighbours is a max_neighbours by points matrix, where max_neighbours
is the maximum number of points neighbouring any other point.  The nth
row of the matrix is a list of the neighbours of n using matlab
indexing i.e. starting from 1, and padded with zeros.

This means that the graph
1
|
2-3

is encoded as

[2 0;
 1 3;
 2 0]

* Lambda is the proportional penalty paid for overlapping regions of
  models. It is a weight between 0 and 1. Set it low to encourage
  areas of overlap, and to 1 to discourage them.

* Internal is a book-keeping vector of length models that describes a
  minimum assignment of interior models. It can be intially set to be
  homogeneously zero, but must be set to the returned value interior
  if you want the code to converge when called repeatedly.

* MDL is a vector of length models describing the minimum description
length prior . The nth element of MDL is the flat cost paid for
introducing model N into a pre-existing solution. 

* Threshold is the outlier rejection term. All costs above Threshold
are trucated to its value.  Set it to Inf if you don't want to use it.


Return Values
-------------
* Models is a points by models matrix showing the weighted assignment of points to models.
Providing lambda=1 it should only contain the values 0 and 1.

* Internal is the previously mentioned book-keeping vector, it should
  be passed back to the multi, if it is called again after model fitting
  in order to guarentee convergence.
 
Sample Usage
------------
Consider arbitary unary costs for 10 variables, each taking 5 states
i.e.
unary=rand(10,5)
we have an empty neighbourhood of the form:
zeros(2,10)

our internal state starts from all zeros
zeros(10,1)

there is equal weight on points in the boundary of overlap
1
(this number describes the weight of the boundary relative to interior points, smaller encourages fitting, large gives larger regions, this does nothing with an empty neighbourhood)

the MDL is 5 random numbers
rand(5,1)
Outlier rejection is off, or set to infinity

Code impletenting this is:

> [models internal]=multi(rand(10,5),zeros(2,10),zeros(10,1),1,rand(5,1),Inf)

models =

   0   0   0   0   1
   0   0   1   0   0
   0   0   1   0   0
   1   0   0   0   0
   0   0   0   0   1
   0   0   0   0   1
   1   0   0   0   0
   1   0   0   0   0
   0   0   1   0   0
   0   0   0   0   1

internal =

   4
   2
   2
   0
   4
   4
   0
   0
   2
   4

the second run (if retraining unary potentials as discussed in the paper) should take the form:

[models internal]=multi(rand(10,5),zeros(2,10),internal,1,rand(5,1),Inf)
